# PIC18_FAT32
Writes to a FA32 SD card!

## Requiremtens 

### Software
- MPLAB X IDE v6.00
- XC8 v2.36
- MCC v5 5.19 (Core v5.4.4)
- FatFs FAT System v1.1.0
- SD/MMC Card 1.1.0

### Hardware
- PIC18F47K42
- SD Card module
- LED
- 3.3V power supply

### Connections

- SD Cards module

    - RC3 -> SDCard SCK
    - RC4 -> SDCard MISO
    - RC5 -> SDCard MOSI
    - RD3 -> SDCard CS
- LED
    - RB5 -> LED
